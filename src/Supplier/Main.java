package Supplier;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Random;
import java.util.function.Supplier;

public class Main {

    public static void main(String[] args) {

        Supplier<Integer[]> supplier = () -> {
            Random random = new Random();
            Integer[] tab = new Integer[10];
            for (int i = 0; i < 10; i++){
                tab[i] = random.nextInt(100);            }
            return tab;
        };

        for(Integer tmp : supplier.get()){
            System.out.println(tmp);
        }

        System.out.println("-------------");
        Supplier<DayOfWeek> supplier1 = () -> {
            return LocalDate.now().getDayOfWeek();
        };

        System.out.println(supplier1.get());

        System.out.println("-------------");
        Supplier<String> supplier2 = () -> {
            LocalDate date = LocalDate.now();
            DateTimeFormatter formatters = DateTimeFormatter.ofPattern("dd/MM/uuuu");
            String text = date.format(formatters);
            return text;
        };

        System.out.println(supplier2.get());

        System.out.println("-------------");
        Supplier<Character> supplier3 = () -> {

            Random random = new Random();
            char a = (char)(random.nextInt(100)+58);

            return a;
        };

        System.out.println(supplier3.get());
    }
}
