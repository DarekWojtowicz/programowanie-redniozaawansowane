package Predicate;

import Consumer.User;

import java.util.Map;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PredicateExecutor {
    public static void main(String[] args) {
        Integer no = 32;
        Integer year = 2000;
        String phoneNo = "+48123456789";

        User user = new User("Adam", "Nowak", 17);

        Predicate<Integer> isOdd = (Integer i) -> {
            return i % 2 == 0;
        };
        System.out.println(isOdd.test(no));

        Predicate<Integer> isLeap = (Integer i) -> {
            if (((i % 4 == 0) && (i % 100 != 0)) || (i % 400 == 0)) {
                return true;
            } else {
                return false;
            }
        };

        System.out.println(isLeap.test(year));

        Predicate<User> isAdult = (User u) -> {
            if (u.age >= 18) {
                return true;
            }else {
                return false;
            }
        };

        System.out.println(isAdult.test(user));

        Predicate<String> isValidPhoneNo = (String s) -> {
            Pattern pattern = Pattern.compile("^\\+48(.){9}");
            Matcher matcher = pattern.matcher(s);
            return matcher.matches();
        };

        System.out.println(isValidPhoneNo.test(phoneNo));
    }
}
