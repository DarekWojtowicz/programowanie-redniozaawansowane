package StreamAPI;

public class Car {
    public String carName;
    public int speed;

    public Car(String carName, int speed) {
        this.carName = carName;
        this.speed = speed;
    }

    @Override
    public String toString() {
        return "Car{" +
                "carName='" + carName + '\'' +
                ", speed=" + speed +
                '}';
    }
}
