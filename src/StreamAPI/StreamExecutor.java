package StreamAPI;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class StreamExecutor {
    public static void main(String[] args) {
        List<String> lista = Arrays.asList("Piotr", "Joanna", "Krzysztof");
        lista.stream().forEach((String s) -> System.out.println(s));
        System.out.println("******************");

        lista.stream().forEach((String s) -> System.out.println(s.length()));

        System.out.println("******************");

        lista.stream().forEach((String s) -> {
            if (s.length() % 2 == 0) {
                System.out.println(s + " długość imienia parzysta");
            } else {
                System.out.println(s + " długość imienia nieparzysta");
            }
        });

        System.out.println();
        System.out.println("****************");

        List<String> listOfNames = Arrays.asList("PiOTr", "joANNa", "krzYsztof");
        listOfNames.stream()
                .map((String s) -> s = s.toUpperCase())
                .forEach((String s) -> System.out.println(s));

        System.out.println();
        System.out.println("****************");

        List<Car> cars = Arrays.asList(new Car("VW", 250), new Car("Audi", 300));
        List<Integer> speed = new ArrayList<Integer>();

        speed = cars.stream()
                .map((Car car) -> {
                    return car.speed;
                })
                .collect(Collectors.toList());

        speed.stream()
                .forEach((Integer i) -> System.out.println(i));

//        List<String> listCSV = Arrays.asList("VW,50","Seat,100","Skoda,150");
//        listCSV.stream()
//                .map((String s)-> {return String[] tab = s.split(",")})

        List<Integer> listOfInts =Arrays.asList(12,13,16);
        listOfInts.stream()
                .filter((Integer i)-> i % 2 == 0)
                .forEach((Integer i)-> System.out.println(i));

        System.out.println();

        List<Rectangle> rectangles = Arrays.asList(new Rectangle(4,4), new Rectangle(6,8));
        rectangles.stream()
                .map((Rectangle r) -> {
                    return r.a * r.b;
                })
                .filter((Integer i) -> i > 30)
                .forEach((Integer i) -> System.out.println(i));

        System.out.println();

        rectangles.stream()
                .filter((Rectangle r) -> r.a == r.b)
                .forEach((Rectangle r) -> System.out.println(r));

        List<Movie> movies = Arrays.asList(new Movie("Lśnienie", "horror", 120, new LocalDate.of(1989,January
                , 1)));
    }
    class Movie{
        String title;
        String type;
        long duration;//min
        LocalDate releaseDate;

        public Movie(String title, String type, long duration, LocalDate releaseDate) {
            this.title = title;
            this.type = type;
            this.duration = duration;
            this.releaseDate = releaseDate;
        }

        @Override
        public String toString() {
            return "Movie{" +
                    "title='" + title + '\'' +
                    ", type='" + type + '\'' +
                    ", duration=" + duration +
                    ", releaseDate=" + releaseDate +
                    '}';
        }
    }
}