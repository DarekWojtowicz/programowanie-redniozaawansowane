package Consumer;

import java.util.function.Consumer;

public class ConsumerExecutor {
    public static void main(String[] args) {
        String time = "22:14";
        User user = new User("Adam", "Małysz", 33);

        Consumer<String> timePresenter = (String s) -> {
            String[] out;
            out = s.split(":");
            System.out.println("Godzina: " + out[0]);
            System.out.println("Minuta: " + out[1]);
        };
        timePresenter.accept(time);


        Consumer<User> userUppercase = (User u) -> {
            u.name = u.name.toUpperCase();
            u.lastName = u.lastName.toUpperCase();
        };

        System.out.println(user);
        userUppercase.accept(user);
        System.out.println(user);

        Consumer<User> userPresenter = (User u) -> {
            System.out.println("*****************");
            System.out.println("Imię:"+ u.name);
            System.out.println("Nazwisko: " + u.lastName);
            System.out.println("Wiek: " + u.age);
            System.out.println("*****************");
        };

        userPresenter.accept(user);
    }
}