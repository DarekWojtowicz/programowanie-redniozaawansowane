package Interface_Funkcyjny;

public class Box {
    public Integer value;

    public Box(Integer value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Box{" +
                "value=" + value +
                '}';
    }
}
