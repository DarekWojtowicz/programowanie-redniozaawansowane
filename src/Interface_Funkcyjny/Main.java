package Interface_Funkcyjny;

import java.util.function.Function;

public class Main {
    public static void main(String[] args) {

        Function<String, Acccount> function = (String input) -> {
            Acccount account = new Acccount(input);
            return account;
        };

        Function<Integer, Box> function1 = (Integer input) -> {
            Box box = new Box(input);
            return box;
        };

        Acccount account = function.apply("Wojtek");
        System.out.println(account);

        Box box = function1.apply(123);
        System.out.println(box);

    }
}
