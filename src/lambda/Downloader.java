package lambda;

public interface Downloader {
    void download(String file);
}
