package lambda;

public interface ToUpperCase {
    String invoke(String input);
}
