package lambda;

public class Main {

    public static void main(String[] args) {

        ToUpperCase sample = (String arg) -> {
            return arg.toUpperCase();
        };
        System.out.println(sample.invoke("qwerty"));

        Converter converter = (String input) -> {
            return Integer.valueOf(input);
        };
        System.out.println(converter.convert("12309"));

        Downloader downloader = (String arg) -> {
            System.out.println("Otwieram plik " + arg);
            System.out.println("Pamiętaj o zamknięciu po obsłużeniu!");
        };
        downloader.download("c:\\input_file.txt");

        Calculator calculator = (int a, int b) -> {
          return a + b;
        };
        System.out.println(calculator.calculate(12,33));
    }
}
